#ifndef INIT_H
#define INIT_H

#include "stm32f1xx.h"

extern uint16_t buf_rx_counter;

//extern uint8_t buf_tx[2][1000];
//extern uint16_t counter_tx;
//extern uint16_t count_tx;
extern uint16_t buf_tx_counter;

typedef struct{
    uint8_t buf[2000];
    uint16_t counter;
    uint16_t count;
}UART_S;

extern UART_S rx[2];
extern UART_S tx[2];

void initUSART(void);

#endif // INIT_H

