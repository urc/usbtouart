#include "init.h"
#include "stm32f1xx_hal.h"

uint16_t buf_rx_counter;
UART_S rx[2];

void initUSART(void){
    RCC->APB1ENR |= RCC_APB1ENR_USART3EN | RCC_APB1ENR_TIM2EN;
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN;
    GPIOB->CRH &= ~(GPIO_CRH_CNF12 | GPIO_CRH_MODE12); // сброс
    GPIOB->CRH |= GPIO_CRH_MODE12; //  10MHz
    // PB10 - tx PB11 - rx
    GPIOB->CRH &= ~(GPIO_CRH_CNF11 | GPIO_CRH_MODE11 | GPIO_CRH_CNF10 | GPIO_CRH_MODE10); // сброс
    GPIOB->CRH |= GPIO_CRH_MODE10_0 | GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1; //  10MHz Input with pull-up / pull-down
    USART3->CR1 = USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE;// | USART_CR1_TXEIE | USART_CR1_TCIE
    USART3->CR2 = 0;
    USART3->CR3 = 0;
    USART3->BRR = 0x138; // BaudRate = 115200 0x271; // BaudRate = 57600 0x4E2 0x138
    USART3->CR1 |= USART_CR1_UE;
//    TIM2->PSC=(SystemCoreClock/1000000)-1; // 1 тик 1 мкс
    TIM2->PSC = (SystemCoreClock/1000000)-1; // 1 тик 1 мкс
    TIM2->ARR = 1000; // цикл 1 мс
    TIM2->CR1 = TIM_CR1_ARPE;
    TIM2->DIER = TIM_DIER_UIE;
    TIM2->EGR = TIM_EGR_UG;
//    TIM2->CR1 |= TIM_CR1_CEN;
    HAL_NVIC_SetPriority(USART3_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);
    HAL_NVIC_SetPriority(TIM2_IRQn, 4, 0);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
}
